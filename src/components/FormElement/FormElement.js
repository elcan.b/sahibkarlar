import React from "react";

import style from "./formElement.module.css";

const FormElement = ({ field, form: { touched, errors }, ...props }) => {
  return (
    <div className={style.inputWrapper}>
      <label htmlFor="">{props.label}</label>
      <input
        type={props.type}
        placeholder={props.placeholder}
        name={props.name}
        {...field}
        style={
          errors[field.name] && touched[field.name] && { borderColor: "red" }
        }
      />
      <p className={style.error}>
        {errors[field.name] && touched[field.name] && errors[field.name]}
      </p>
    </div>
  );
};

export default FormElement;
