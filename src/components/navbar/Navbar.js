import React from "react";

import Logo from "../../assets/images/logo.svg";
import style from "./navbar.module.css";

const Navbar = () => {
  return (
    <div className={style.nav}>
      <div className={style.logo}>
        <img src={Logo} alt="" />
      </div>
      <div className={style.navbarMenu}>
        <div className={style.date}>
          <p>Bugün, 22 Yanvar 2018</p>
          <span>Çərşənbə</span>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
