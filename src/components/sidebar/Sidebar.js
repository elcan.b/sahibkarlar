import React from "react";
import { NavLink } from "react-router-dom";

import style from "./sidebar.module.css";
import Partner from "../../assets/images/partnerships.svg";
import Calendar from "../../assets/images/calendar.svg";
import Network from "../../assets/images/network.svg";
import Cooperation from "../../assets/images/cooperation.svg";

const Sidebar = () => {
  return (
    <div className={style.sidebar}>
      <div className={style.sidebarItem}>
        <NavLink to="/">
          <img src={Partner} alt="" />
          <p>Partnyorluq</p>
        </NavLink>
      </div>
      <div className={style.sidebarItem}>
        <NavLink to="/">
          <img src={Calendar} alt="" />
          <p>Tədbirlər</p>
        </NavLink>
      </div>
      <div className={style.sidebarItem}>
        <NavLink to="/">
          <img src={Cooperation} alt="" />
          <p>Partnyorluq </p>
        </NavLink>
      </div>
      <div className={style.sidebarItem}>
        <NavLink to="/">
          <img src={Network} alt="" />
          <p>Faydalı linklər</p>
        </NavLink>
      </div>
    </div>
  );
};

export default Sidebar;
