import React from "react";
import { Row, Col } from "reactstrap";

import style from "../../assets/css/TabStyle.module.css";

const ProductCategory = () => {
  return (
    <div className={style.tabWrapper}>
      <div className={style.heading}>
        <p>İxrac ölkələri</p>
      </div>
      <div className={style.tabContent}>
        <form>
          <Row>
            <Col md="4">
              <label>İşçi sayısı</label>
              <select name="" id="">
                <option value="">1</option>
                <option value="">2</option>
                <option value="">3</option>
              </select>
            </Col>
            <Col md="4">
              <label>İşçi sayısı</label>
              <select name="" id="">
                <option value="">1</option>
                <option value="">2</option>
                <option value="">3</option>
              </select>
            </Col>
            <Col md="4">
              <label>İşçi sayısı</label>
              <select name="" id="">
                <option value="">1</option>
                <option value="">2</option>
                <option value="">3</option>
              </select>
            </Col>
            <Col md="4">
              <label>İşçi sayısı</label>
              <select name="" id="">
                <option value="">1</option>
                <option value="">2</option>
                <option value="">3</option>
              </select>
            </Col>
            <Col md="4">
              <label>İşçi sayısı</label>
              <select name="" id="">
                <option value="">1</option>
                <option value="">2</option>
                <option value="">3</option>
              </select>
            </Col>
            <Col md="4">
              <label>İşçi sayısı</label>
              <select name="" id="">
                <option value="">1</option>
                <option value="">2</option>
                <option value="">3</option>
              </select>
            </Col>
          </Row>
        </form>
      </div>
    </div>
  );
};

export default ProductCategory;
