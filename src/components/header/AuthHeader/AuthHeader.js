import React from "react";

import Logo from "../../../assets/images/sahibkarKlubuLogo.svg";
import style from "./header.module.css";

const AuthHeader = () => {
  return (
    <div className={style.header}>
      <img src={Logo} alt="" />
    </div>
  );
};

export default AuthHeader;
