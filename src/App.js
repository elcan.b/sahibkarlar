import React, { useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Registration from "./container/Auth/Register/Registration";
import PrivateRoute from "./components/common/PrivateRoute";
import Dashboard from "./container/Dashboard/Dashboard";
import Login from "./container/Auth/Login/Login";
import { loadUser } from "./store/action/auth";
import store from "./store";

function App() {
  useEffect(() => {
    store.dispatch(loadUser());
  });
  return (
    <div className="App">
      <Router>
        <Switch>
          <PrivateRoute exact path="/" component={Dashboard} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Registration} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
