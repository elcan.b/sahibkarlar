const initialState = {
  token: localStorage.getItem("token"),
  isAuthenticated: false,
  user: null,
  error: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "LOGIN_SUCCESS":
      localStorage.setItem("token", action.payload[0].password);
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload
      };
    case "USER_LOADED":
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload
      };
    case "LOGIN_FAIL":
      return {
        ...state,
        isAuthenticated: false,
        user: null,
        error: true
      };
    default:
      return state;
  }
};
