import axios from "axios";

export const login = user => dispatch => {
  axios.get("http://localhost:3004/users").then(res => {
    const body = res.data.filter(
      e => e.mail === user.mail && e.password === user.password
    );
    body.length
      ? dispatch({
          type: "LOGIN_SUCCESS",
          payload: body
        })
      : dispatch({
          type: "LOGIN_FAIL"
        });
  });
};

export const register = user => dispatch => {
  const { email, password } = user;

  const body = JSON.stringify({ email, password });

  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  axios
    .post("http://localhost:3004/users", body, config)
    .then(res => console.log(res.data));
};

export const loadUser = () => (dispatch, getState) => {
  const token = getState().auth.token;
  axios.get("http://localhost:3004/users").then(res => {
    const body = res.data.filter(e => e.password === token);
    body.length &&
      dispatch({
        type: "USER_LOADED",
        payload: body
      });
  });
};
