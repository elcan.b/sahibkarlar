import React from "react";
import Navbar from "../../components/navbar/Navbar";
import Sidebar from "../../components/sidebar/Sidebar";
import Profile from "../Profile/Profile";

const Dashboard = () => {
  return (
    <>
      <Navbar />
      <Sidebar />
      <Profile />
    </>
  );
};

export default Dashboard;
