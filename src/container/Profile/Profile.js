import React, { useState } from "react";
import { TabContent, TabPane } from "reactstrap";

import style from "./Profile.module.css";
import CompanyInfo from "../../components/CompanyInfo/CompanyInfo";
import ProductCategory from "../../components/ProductCategory/ProductCategory";

const Profile = () => {
  const [activeTab, setActiveTab] = useState("1");

  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  return (
    <div className={style.profile}>
      <div className={style.tabsWrapper}>
        <div className={style.tabsHeading}>
          <h2>Sahibkar Pofili</h2>
        </div>
        <div className={style.tabs}>
          <button
            className={activeTab === "1" ? style.active : null}
            onClick={() => {
              toggle("1");
            }}
          >
            Şirkət Məlumatları
          </button>
          <button
            className={activeTab === "2" ? style.active : null}
            onClick={() => {
              toggle("2");
            }}
          >
            Məhsul Kateqoiyası
          </button>
          <button
            className={activeTab === "3" ? style.active : null}
            onClick={() => {
              toggle("3");
            }}
          >
            Fəaliyyət sahələri
          </button>
        </div>
      </div>
      <div className={style.tabsContent}>
        <TabContent style={{ width: "100%" }} activeTab={activeTab}>
          <TabPane tabId="1">
            <CompanyInfo />
          </TabPane>
          <TabPane tabId="2">
            <ProductCategory />
          </TabPane>
          <TabPane tabId="3">
            <CompanyInfo />
          </TabPane>
        </TabContent>
      </div>
    </div>
  );
};

export default Profile;
