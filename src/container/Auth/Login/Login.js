import React from "react";
import * as Yup from "yup";
import { connect } from "react-redux";
import { Row, Col } from "reactstrap";
import { Formik, Form, Field } from "formik";
import { Redirect } from "react-router-dom";

import style from "../auth.module.css";
import { login } from "../../../store/action/auth";
import AuthHeader from "../../../components/header/AuthHeader/AuthHeader";
import FormElement from "../../../components/FormElement/FormElement";

const LoginSchema = Yup.object().shape({
  email: Yup.string().required("This field is required"),
  password: Yup.string().required("This field is required")
});

const Login = props => {
  if (props.auth.isAuthenticated) {
    return <Redirect to="/" />;
  }
  return (
    <Row className="clear">
      <Col md="3" className="clear">
        <div className={style.wrapper}>
          <AuthHeader />
          <div className={style.form}>
            <h1>Giriş</h1>
            <Formik
              initialValues={{
                email: "",
                password: ""
              }}
              validationSchema={LoginSchema}
              onSubmit={values => {
                props.login(values);
              }}
            >
              {({ isValid }) => (
                <Form>
                  <Field
                    type="text"
                    name="email"
                    placeholder="example@gmail.com"
                    label="E-Poçt, Vöen, Telefon nömrə"
                    component={FormElement}
                  />
                  <Field
                    label="Parol"
                    type="password"
                    name="password"
                    component={FormElement}
                  />
                  <button disabled={!isValid} type="submit">
                    Daxil ol
                  </button>
                </Form>
              )}
            </Formik>
          </div>
          <div className={style.copyrights}>
            <p>© Copyrights Azərbaycan Beynəlxalq Bankı</p>
          </div>
        </div>
      </Col>
      <Col md="9" className="clear">
        <p className={style.picture}></p>
      </Col>
    </Row>
  );
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { login })(Login);
