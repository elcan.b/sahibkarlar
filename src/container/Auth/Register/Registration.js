import React from "react";
import * as Yup from "yup";
import { connect } from "react-redux";
import { Row, Col } from "reactstrap";
import { Formik, Form, Field } from "formik";
import { withRouter } from "react-router-dom";

import style from "../auth.module.css";
import { register } from "../../../store/action/auth";
import AuthHeader from "../../../components/header/AuthHeader/AuthHeader";
import FormElement from "../../../components/FormElement/FormElement";

const RegistrationSchema = Yup.object().shape({
  email: Yup.string().required("This field is required"),
  password: Yup.string().required("This field is required"),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref("password"), null], `Password doesn't match`)
    .required("You need to confirm your password.")
});

const Registration = props => {
  return (
    <Row className="clear">
      <Col md="3" className="clear">
        <div className={style.wrapper}>
          <AuthHeader />
          <div className={style.form}>
            <h1>Qeydiyyat</h1>
            <Formik
              initialValues={{
                email: "",
                password: "",
                confirmPassword: ""
              }}
              validationSchema={RegistrationSchema}
              onSubmit={values => {
                props.register(values);
                props.history.push("/");
              }}
            >
              {({ isValid }) => (
                <Form>
                  <Field
                    type="text"
                    name="email"
                    placeholder="example@gmail.com"
                    label="E-Poçt, Vöen, Telefon nömrə"
                    component={FormElement}
                  />
                  <Field
                    label="Parol"
                    type="password"
                    name="password"
                    component={FormElement}
                  />
                  <Field
                    type="password"
                    label="Parol təkrarla"
                    name="confirmPassword"
                    component={FormElement}
                  />
                  <button disabled={!isValid} type="submit">
                    Daxil ol
                  </button>
                </Form>
              )}
            </Formik>
          </div>
          <div className={style.copyrights}>
            <p>© Copyrights Azərbaycan Beynəlxalq Bankı</p>
          </div>
        </div>
      </Col>
      <Col md="9" className="clear">
        <p className={style.picture}></p>
      </Col>
    </Row>
  );
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { register })(withRouter(Registration));
